import java.util.*;
import java.util.stream.Collectors;

public class DoubleStack {

   private LinkedList<Double> list;
   private static final ArrayList<String> ALL_OPERATORS = new ArrayList<>
           (Arrays.asList("+", "-", "*", "/", "SWAP", "ROT", "DUP"));
   private static final ArrayList<String> OPERATORS = new ArrayList<>
           (Arrays.asList("+", "-", "*", "/"));
   private static final ArrayList<String> UNARY_OPERATORS = new ArrayList<>
           (Arrays.asList("SWAP", "ROT"));
   private static final ArrayList<String> INCREASE_OPERATORS = new ArrayList<>
           (Arrays.asList("DUP"));

   public static void main (String[] argum) {
      DoubleStack one = new DoubleStack();
      one.push(2.1);
      one.push(3.4);

      System.out.println(one);
   }

   DoubleStack() {
      this.list = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clone = new DoubleStack();
      clone.list.addAll(this.list);
      return clone;
   }

   public boolean stEmpty() {
      return this.list.isEmpty();
   }

   public void push (double a) {
      this.list.add(a);
   }

   public double pop() {
      try {
         return this.list.removeLast();
      } catch (NoSuchElementException e) {
         throw new RuntimeException("Underflow - stack is empty!");
      }
   }

   public void op (String s) {
      try {

         switch (s) {
            case "+":
               double a = this.list.removeLast();
               this.list.add(pop() + a);
               break;
            case "-":
               a = this.list.removeLast();
               this.list.add(pop() - a);
               break;
            case "*":
               a = this.list.removeLast();
               this.list.add(pop() * a);
               break;
            case "/":
               a = this.list.removeLast();
               this.list.add(pop() / a);
               break;
            case "SWAP":
               try {
                  swap();
                  break;
               } catch (Exception e) {
                  throw new RuntimeException("Not enough elements to swap!");
               }
            case "ROT":
               try {
                  rot();
                  break;
               } catch (Exception e) {
                  throw new RuntimeException("Not enough elements to swap!");
               }
            case "DUP":
               try {
                  dup();
                  break;
               } catch (Exception e) {
                  throw new RuntimeException("Not enough elements to swap!");
               }
            default:
               throw new RuntimeException(s + " is not a valid operator!");
         }
      } catch (RuntimeException e) {
         throw new RuntimeException("Underflow - not enough elements for " + s + " in stack!");
      }
   }

   private void dup() {
      try {
         Double a = this.list.removeLast();
         this.list.add(a);
         this.list.add(a);
      } catch (Exception e) {
         throw new RuntimeException("No elements in stack!");
      }
   }


   private void rot() {
      try {
         Double c = this.list.removeLast();
         Double b = this.list.removeLast();
         Double a = this.list.removeLast();
         this.list.add(b);
         this.list.add(c);
         this.list.add(a);
      } catch (Exception e) {
         throw new RuntimeException("Not enough elements to rotate!!");
      }
   }

   private void swap() {
      try {
         Double a = this.list.removeLast();
         Double b = this.list.removeLast();
         this.list.add(a);
         this.list.add(b);
      } catch (Exception e) {
         throw new RuntimeException("Not enough elements to swap!");
      }
   }

   public double tos() {
      try {
         return this.list.getLast();
      } catch (NoSuchElementException e) {
         throw new RuntimeException("Underflow - stack is empty!");
      }
   }

   @Override
   public boolean equals (Object o) {
      if (o instanceof DoubleStack) {
         DoubleStack other = (DoubleStack) o;
         return this.list.hashCode() == other.list.hashCode();
      }
      return false;
   }

   @Override
   public String toString() {
//      return list.stream()
//              .map(x -> x.toString())
//              .collect(Collectors.joining(" "));

      StringBuilder str = new StringBuilder();
      for (double number : this.list) {
         str.append(number).append(" ");
      }
      return str.toString().trim();
   }

   /**
    * Checks if numbers and mathematical operators are in right proportion to
    * carry out math operations by RPN rules. If not, throws Runtime exception.
    *
    * @param args list of arguments math operations to be carried out with
    * @param pol arguments in String form
    */
   private static void checkAmountOfOperators(ArrayList<String> args, String pol) {
      int operatorsCount = 0;
      int unary_operators = 0;

      for (String arg : args) {
         if (OPERATORS.contains(arg)) {
            operatorsCount++;
         } else if (UNARY_OPERATORS.contains(arg)){
            unary_operators++;
         }
      }

      if (args.size() - unary_operators < 2 * operatorsCount + 1) {
         throw new RuntimeException("Not enough numbers in : " + pol);
      } else if (args.size() - unary_operators > 2 * operatorsCount + 1) {
         throw new RuntimeException("Not enough operators in : " + pol);
      }
   }

   /**
    * Returns ArrayList object that contains String elements without whitespace characters.
    *
    * Method divides String argument into smaller String objects where any whitespaces appear
    * in argument. Throws Runtime exception if argument is null, empty String or consists
    * only whitespace characters.
    *
    * @param pol any String
    * @return ArrayList of Strings
    */
   private static ArrayList<String> fractionateAndCleanString(String pol) {
      ArrayList<String> arguments = new ArrayList<>();
      String[] args;

      try {
         args = pol.split("\\s+");
      } catch (NullPointerException e) {
         throw new RuntimeException("Given argument is null!");
      }

      if (pol.isEmpty()) {
         throw new RuntimeException("'" + pol + "' is empty!");
      }

      for (String arg : args) {
         if (arg.trim().length() > 0) {
            arguments.add(arg.trim());
         }
      }
      if (arguments.isEmpty()) {
         throw new RuntimeException("'" + pol + "' consists only whitespace characters!");
      }
      return arguments;
   }

   public static double interpret (String pol) {
      DoubleStack stack = new DoubleStack();
      ArrayList<String> cleanedArguments = fractionateAndCleanString(pol);
      checkAmountOfOperators(cleanedArguments, pol);

      for (String arg : cleanedArguments) {
         if (ALL_OPERATORS.contains(arg)) {
            stack.op(arg);
         } else {
            try {
               stack.push(Double.parseDouble(arg));
            } catch (NumberFormatException e) {
               throw new RuntimeException(arg + " in + '" + pol + "' is not valid operator or number!");
            }
         }
      }
      return stack.pop();
   }
}
